# simptask

A Simple Task Flutter project.
by Rifani F
22 Oct 2022

## Interactive Mockup URL

Interactive mockup with button link available at: https://simptaskbyrfmockup.netlify.app/

## Live App URL

Please check the live app at: https://simptaskbyrf.netlify.app/#/

## Test Log
PS C:\Users\rifan\Documents\FlutterProjects\simptask> flutter test -r expanded
00:00 +0: Add task item test
00:00 +1: Toggle task item status test
00:00 +2: Edit task item title test
00:00 +3: Edit task item priority test
00:00 +4: Delete a task item test
00:00 +5: All tests passed!

## App screenshots

![Homepage](/resources/Screenshot Homepage.png)

![Edit Task](/resources/Screenshot Edit Task.png)

![Delete Task](/resources/Screenshot Delete Task.png)