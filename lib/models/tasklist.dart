/// SimpTask Task List Management by RF
/// created Oct 22, 2022
/// Simple task management using local storage

import 'package:localstorage/localstorage.dart';

/// enum for task priority
enum Priority { Low, Medium, High }

/// the task item model class
class TaskItem {
  String title;
  bool done;
  Priority priority = Priority.Low;

  TaskItem({required this.title, required this.done, required this.priority});

  /// encode an item to json encodable
  toJSONEncodable() {
    Map<String, dynamic> m = Map();

    m['title'] = title;
    m['done'] = done;
    m['priority'] = priority.toString();

    return m;
  }

  /// convert priority enum value to string
  String priorityString() {
    return priority.toString().substring(priority.toString().indexOf('.') + 1);
  }
}

/// the class for managing the task list
class TaskList {
  List<TaskItem> items = [];
  final LocalStorage storage = LocalStorage('task_app');
  bool titleAscending = false;
  bool priorityAscending = false;

  /// encode items to json encodable
  toJSONEncodable() {
    return items.map((item) {
      return item.toJSONEncodable();
    }).toList();
  }

  /// save data to local storage
  saveToStorage() {
    storage.setItem('tasks', toJSONEncodable());
  }

  /// clear local storage data
  clearStorage() async {
    await storage.clear();
    items = storage.getItem('tasks') ?? [];
  }

  /// initialize local storage
  init() {
    var items = storage.getItem('tasks');

    if (items != null) {
      items = List<TaskItem>.from(
        (items as List).map(
          (item) => TaskItem(
            title: item['title'],
            done: item['done'],
            priority: Priority.values
                .firstWhere((e) => e.toString() == item['priority']),
          ),
        ),
      );
    }
  }

  /// toggle task item status open <-> done
  toggleStatus(TaskItem item) async {
    item.done = !item.done;
    await saveToStorage();
  }

  /// add a task item to task item list and save
  addItem(String title) async {
    final item = TaskItem(title: title, done: false, priority: Priority.Low);
    items.add(item);
    await saveToStorage();
  }

  /// edit task item title and save
  editItemTitle(TaskItem item, String title) async {
    item.title = title;
    await saveToStorage();
  }

  /// edit task item priority and save
  editItemPriority(TaskItem item, Priority priority) async {
    item.priority = priority;
    await saveToStorage();
  }

  /// delete a task item and save
  deleteItem(TaskItem item) async {
    items.remove(item);
    await saveToStorage();
  }

  /// sort task item list by title and save
  sortItemByTitle() async {
    if (titleAscending) {
      items.sort((a, b) => a.title.compareTo(b.title));
    } else {
      items.sort((a, b) => b.title.compareTo(a.title));
    }
    titleAscending = !titleAscending;
    await saveToStorage();
  }

  /// sort task item list by title and save
  sortItemByPriority() async {
    if (priorityAscending) {
      items.sort((a, b) => a.priority.index.compareTo(b.priority.index));
    } else {
      items.sort((a, b) => b.priority.index.compareTo(a.priority.index));
    }
    priorityAscending = !priorityAscending;
    await saveToStorage();
  }

  // count the available tasks
  int getTaskOpenCount() {
    return (items.where((item) => item.done == false)).length;
  }

  // count the finished tasks
  int getTaskDoneCount() {
    return (items.where((item) => item.done == true)).length;
  }
}
