/// SimpTask Main by RF
/// created Oct 22, 2022
/// Simple task management using local storage

import 'package:flutter/material.dart';
import 'package:simptask/views/homepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SimpTask',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}
