import 'package:flutter/material.dart';
import 'package:simptask/models/tasklist.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  MyHomePageState createState() => MyHomePageState();
}

/// the homepage class
class MyHomePageState extends State<HomePage> {
  /// initialize task list
  final TaskList list = TaskList();
  bool initialized = false;

  /// create text editing controller for add new task and edit task
  TextEditingController controller = TextEditingController();
  TextEditingController editTitleController = TextEditingController();

  /// create list for Priority enum
  final List<String> priorityList = Priority.values.map((e) => e.name).toList();

  /// convert Priority enum value to string
  String _priorityString(Priority priority) {
    return priority.toString().substring(priority.toString().indexOf('.') + 1);
  }

  /// toggle task item status open <-> done
  _toggleItem(TaskItem item) {
    setState(() {
      list.toggleStatus(item);
    });
  }

  /// add a task item to task item list
  _addItem(String title) {
    setState(() {
      list.addItem(title);
    });
    controller.clear();
  }

  /// edit task item title
  _editItemTitle(TaskItem item, String title) {
    setState(() {
      list.editItemTitle(item, title);
    });
  }

  /// edit task item priority
  _editItemPriority(TaskItem item, Priority priority) {
    setState(() {
      list.editItemPriority(item, priority);
    });
  }

  /// delete a task item
  _deleteItem(TaskItem item) {
    setState(() {
      list.deleteItem(item);
    });
  }

  /// sort task item list by title
  _sortItemByTitle() {
    setState(() {
      list.sortItemByTitle();
    });
  }

  /// sort task item list by title
  _sortItemByPriority() {
    setState(() {
      list.sortItemByPriority();
    });
  }

  // display edit task title dialog
  Future<void> _displayTextInputDialog(
      BuildContext context, TaskItem item) async {
    editTitleController.text = item.title;
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Edit Task Title'),
          content: TextField(
              //initialValue: item.title,
              controller: editTitleController,
              decoration:
                  const InputDecoration(hintText: "Edit the task title..."),
              onEditingComplete: () {
                _editItemTitle(item, editTitleController.text);
                Navigator.pop(context);
              }),
          actions: <Widget>[
            TextButton(
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                _editItemTitle(item, editTitleController.text);
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  // display delete item confirmation dialog
  Future<void> _displayDeleteConfirmDialog(
      BuildContext context, TaskItem item) async {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Delete Item "' + item.title + '?'),
          actions: <Widget>[
            TextButton(
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                _deleteItem(item);
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  // display delete all items confirmation dialog
  Future<void> _displayDeleteAllConfirmDialog(BuildContext context) async {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Delete all items (clear storage)?'),
          actions: <Widget>[
            TextButton(
              child: const Text('CANCEL'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                list.clearStorage();
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  /// display the main page
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SimpTask by RF'),
        backgroundColor: Colors.deepOrange,
      ),
      body: Container(
          padding: const EdgeInsets.all(10.0),
          constraints: const BoxConstraints.expand(),
          child: FutureBuilder(
            future: list.storage.ready,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.data == null) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }

              // initialize the task item list local storage
              if (!initialized) {
                list.init();
                initialized = true;
              }

              // create widget list to display the task list
              List<Widget> widgets = list.items.map((item) {
                return ListTile(
                  // create the task status checkbox
                  leading: Checkbox(
                    value: item.done,
                    onChanged: (_) {
                      _toggleItem(item);
                    },
                  ),
                  // create the task title
                  title: Text(item.title),
                  selected: item.done,
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      // create the dropdown button to select task priority
                      DropdownButton(
                        value: item.priorityString(),
                        items: priorityList
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        onChanged: (value) {
                          _editItemPriority(
                              item,
                              Priority.values.firstWhere(
                                  (e) => _priorityString(e) == value));
                        },
                      ),
                      // create the edit icon button to open the task title edit dialog
                      IconButton(
                        icon: const Icon(Icons.edit),
                        onPressed: () {
                          //_editItemTitle(item, item.title + "_edit");
                          _displayTextInputDialog(context, item);
                        },
                        tooltip: 'Edit task...',
                      ),
                      // create the delete icon button to delete selected item
                      IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () {
                          //_deleteItem(item);
                          _displayDeleteConfirmDialog(context, item);
                        },
                        tooltip: 'Delete task...',
                      )
                    ],
                  ),
                );
              }).toList();

              // count the available tasks
              //var taskOpenCount =
              //    (list.items.where((item) => item.done == false)).length;
              // count the finished tasks
              //var taskDoneCount =
              //    (list.items.where((item) => item.done == true)).length;

              return Column(
                children: <Widget>[
                  // create a list tile for new task input
                  ListTile(
                    title: TextField(
                      autofocus: true,
                      controller: controller,
                      decoration: const InputDecoration(
                        labelText: 'New task title...',
                      ),
                      onEditingComplete: () {
                        _addItem(controller.value.text); //_save,
                      },
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        IconButton(
                          icon: const Icon(Icons.add),
                          onPressed: () {
                            _addItem(controller.value.text);
                          },
                          tooltip: 'Add task',
                        ),
                        IconButton(
                          icon: const Icon(Icons.delete),
                          onPressed: () {
                            _displayDeleteAllConfirmDialog(context);
                          },
                          tooltip: 'Delete all tasks...',
                        )
                      ],
                    ),
                  ),
                  ListTile(
                    leading: const Text("Status"),
                    title: TextButton(
                      child: const Text('Title'),
                      onPressed: () {
                        _sortItemByTitle();
                      },
                    ),
                    trailing: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        TextButton(
                          child: const Text('Priority'),
                          onPressed: () {
                            _sortItemByPriority();
                          },
                        ),
                        const SizedBox(width: 100),
                      ],
                    ),
                  ),
                  // create the area to display the task item list
                  Expanded(
                    flex: 1,
                    child: ListView(
                      children: widgets,
                      itemExtent: 50.0,
                    ),
                  ),
                  // create task count info text
                  ListTile(
                      title: Text("Open: " +
                          list.getTaskOpenCount().toString() +
                          ", Done: " +
                          list.getTaskDoneCount().toString())),
                ],
              );
            },
          )),
    );
  }
}
