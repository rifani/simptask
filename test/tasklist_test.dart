import 'package:flutter_test/flutter_test.dart';
import 'package:simptask/models/tasklist.dart';
import 'package:collection/collection.dart';

void main() {
  final TaskList list = TaskList();
  list.init();
  list.clearStorage();
  test('Add task item test', () async {
    var numTask = list.items.length;
    await list.addItem("New Task");
    expect(list.items.length, numTask + 1);
  });

  test('Toggle task item status test', () async {
    // add at least 1 item
    if (list.items.isEmpty) {
      list.items.add(
          TaskItem(title: "New Task", done: false, priority: Priority.Low));
    }
    var item = list.items[0];
    var status = item.done;
    await list.toggleStatus(item);
    expect(item.done, !status);
  });

  test('Edit task item title test', () async {
    // add at least 1 item
    if (list.items.isEmpty) {
      list.items.add(
          TaskItem(title: "New Task", done: false, priority: Priority.Low));
    }
    var item = list.items[0];
    await list.editItemTitle(item, "New Task Update");
    expect(item.title, "New Task Update");
  });

  test('Edit task item priority test', () async {
    // add at least 1 item
    if (list.items.isEmpty) {
      list.items.add(
          TaskItem(title: "New Task", done: false, priority: Priority.Low));
    }
    var item = list.items[0];
    await list.editItemPriority(item, Priority.High);
    expect(item.priority, Priority.High);
  });

  test('Delete a task item test', () async {
    // add at least 1 item
    if (list.items.isEmpty) {
      list.items.add(
          TaskItem(title: "New Task", done: false, priority: Priority.Low));
    }
    var item = list.items[0];
    var numTask = list.items.length;
    await list.deleteItem(item);
    expect(list.items.length, numTask - 1);
  });
}
